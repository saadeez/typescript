/*
    GREETING Function:
*/
var a = "Saad";
function greet(name) {
    var b = "Hello, ";
    document.writeln("<p>" + b + name + "</p>");
    //return res;
}
greet(a);
/*
    SUM Function:
*/
var one = 1, two = 2;
function sum(p1, p2) {
    var res = p1 + p2;
    document.writeln("<p>" + res + "</p>");
}
sum(one, two);
/*
    ARRAY Function:
*/
var number = [1, 2, 3];
function getArr(pArr) {
    document.write("<ul>");
    for (var index = 0; index < pArr.length; index++) {
        var element = pArr[index];
        document.write("<li>" + element + "</li>");
    }
    document.write("</ul>");
}
getArr(number);
var student = {
    name: ['Saad', 'Suffi', 'Tanno'],
    ID: 112,
    Class: "2",
};
function studArr(sArr) {
    document.write("<ul>");
    for (var key in sArr) {
        if (sArr.hasOwnProperty(key)) {
            var element = sArr[key];
            document.write("<li>" + element + "</li>");
        }
    }
    document.write("</ul>");
}
studArr(student.name);
//# sourceMappingURL=main.js.map