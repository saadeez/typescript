/*var a = 1;
a = "Saad";

console.log(a);
*/

var mystring : string = "Saad";
var myboolean : Boolean = true;

var a = 1;
var b = "1";

if (a.toString() === b) {
	console.log("true");
}
else{
	console.log("false");
}

var obj = {
    name : "Saad", 
    id : 2
};

obj = {
    name : "Saad",
    id : 2,
    address : "Gulshan"

};

obj = {
    address : "Gulshan",
    name : "Saad",
    id : 2
};

var allType : any = "Saad";
allType = 2;

obj = <any> {				// for temporary typecasting
    address : "Gulshan",
    name : "Saad",
    id : 2
};